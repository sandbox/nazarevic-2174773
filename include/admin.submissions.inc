<?php
define("SUBMISSION_STATUS_DEFAULT", 0);

function admin_webform_admininster_submission_form($form, &$form_state, $nid, $sid) {
	$form = array();

	$form['sid'] = array(
		'#type' => 'hidden',
		'#default_value' => $sid,
	);

	$status = AdminWebformSubmission::IsSubmissionExists($sid) ? 'posted' : 'unposted';

	$form['status'] = array(
		'#prefix' => '<div id="sstatus">',
		'#markup' => $status,
		'#suffix' => '</div>',
	);

	$form['post-submission'] = array(
		'#type' => 'button',
		'#value' => 'post',
		'#ajax' => array(
			'callback' => 'admin_webform_post_submission',
			'wrapper'  => 'sstatus',
		),
	);

	$form['unpost-submission'] = array(
		'#type' => 'button',
		'#value' => 'unpost',
		'#ajax'	 => array(
			'callback' => 'admin_webform_unpost_submission',
			'wrapper'  => 'sstatus',
		),
	);

	return $form;
}

function admin_webform_post_submission($form, &$form_state) {
   
	try {
            $s = admin_webform_submission($form_state['values']['sid']);
            $message = t("Succesfully posted");

            if($s ->IsPosted()) {
                $message = t('Already posted');           
            }
            else {
                $s ->ChangeStatus('post');
            }       

            $commands = array();
            $commands[] = ajax_command_html('#sstatus', $message);
	
            return array('#type' => 'ajax', '#commands' => $commands);
        }
	catch(Exception $e) {
		drupal_set_message("$e");
		return "Error occured. Contact us. $e";
	}

}

function admin_webform_unpost_submission($form, &$form_state) {

	try	{
            $s = admin_webform_submission($form_state['values']['sid']);
            $message = t('Succesfully unposted');

            if($s ->IsPosted() == FALSE) {
                $message = t('Already unposted');
            }
            else {
                $s ->ChangeStatus('unpost');
            }        

            $commands = array();
            $commands[] = ajax_command_html('#sstatus', $message);

            return array('#type' => 'ajax', '#commands' => $commands);
	}
	catch(Exception $e) {
                dsm("$e");
		return "Error occured. Contact us. $e";
	}
	
}

function admin_webform_admin_submission_access() {
	return user_access('administer submissions');
}