<?php
function admin_webform_comment_submission_form($form, &$form_state, $sid) {
	
	$form['sid'] = array(
		'#type' => 'hidden',
		'#default_value' => $sid,
	);

	$form['comment'] = array(
		'#type' => 'textarea',
	);
        
        $form['status'] = array(
            '#prefix' => '<div id="status">',
            "#suffix" => '</div>',
        );

	$form['leave-comment'] = array(
                '#type' => 'button',
                '#value' => t("Leave comment"),
                '#ajax' => array(
                        'callback' => 'ajax_leave_comment',
                ),
	);

	return $form;
}


function admin_webform_leave_comment($form, &$form_state) {
    $values= $form_state['values'];
    $sid = $values['sid'];
    $comment = $values['comment'];  
    
    try {
        admin_webform_submission($sid)
        ->AddComment($comment);
        drupal_set_message("Success");

        $commands = array();
        $commands[] = ajax_command_html('#status', 'success');

        return array('#type' => 'ajax', '#commands' => $commands);
    }
    catch(Exception $e) {
        drupal_set_message("Contact us $e");
    }    
    
}