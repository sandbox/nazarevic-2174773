<?php
require_once 'order.form.php';
require_once 'comment.form.php';
require_once 'admin.submissions.inc';
require_once 'constants.php';

class AdminWebformSubmission {

    private  $_sid;//submission id
    private  $_nid;//submission node id
    private  $_users;//array of users aloved to see submission
    private  $_comment;//comment of submission
    private  $_type;//type of submissioon
    private  $_isNew;//is submission new
    private  $_isPosted;//is submission in TABLE_NAME table
    private  $_submission;//stdClass submission object

    public function __construct($sid, $type =  NEW_SUBMISSION) {
            $tmp =  self::GetSubmission($sid);

            //if submission is'nt exits in database( unposted )
            if($tmp == FALSE) {
                    $this ->_sid 	= $sid;
                    $this ->_type 	= $type;
                    $this ->_isPosted 	= FALSE;
                    $this ->_users  = array();
            }
            else {
                    $this ->_sid 	 = $tmp ->sid;
                    $this ->_users 	 = $tmp ->users;
                    $this ->_type 	 = $tmp ->type;
                    $this ->_comment = $tmp ->comment;
                    $this ->_isPosted 	 = TRUE;

            }
            
            $this ->_nid 	= self::GetNid($sid);
            $this ->_isNew  = ($type ==  NEW_SUBMISSION) ? TRUE : FALSE;
            $submission = $this ->SubmissionQuery() ->execute() ->fetchObject();
            $this ->_submission = $submission;
    }

    //---- GETTERS ------
    public function GetSid()   { return $this ->_sid;  }
    public function GetUsers() { return $this ->_users;}
    public function GetType()  { return $this ->_type; }
    public function IsPosted()   { return $this ->_isPosted; }
    public function IsNew()    { return $this ->GetType() ==  NEW_SUBMISSION; }

    public function SetType($value) {
            $this ->_type = $value; 
            return $this; 
    }	

    public function UsersCount() {return count($this ->_users); }

    public function GetSubmissionOrderForm() {
    return render(drupal_get_form('admin_webform_submission_order_form', $this ->_sid));
    }

    public function SubmissionQuery() {
    $query = db_select(admin_webform_get_view_name($this ->_nid), 't')
            ->orderBy('t.submitted', "DESC")
            ->condition('sid', $this ->_sid);		
            $query->fields('t');

            return $query;
    }

    private function ViewSubmission() {
        $output = "";
        foreach ($this ->_submission as $key => $value) {		
                $output .=
                "<div class=\"$key\">" .
                t($key) . ": "
                . admin_webform_translate_key($key, $value, $this ->_nid)
                . "</div>";
        }
        return $output;
    }

    /**
     * public function ToHtml($nid = NULL, $admin = FALSE)
     * 
     * @return html {string} html view of submission
     */
    public function ToHtml($u = NULL) {	
        if($u == NULL) {
            global $user;
            $u = $user;
        }

        if(!$this ->CanUserAccess($u)) {
            return $this ->GetSubmissionOrderForm();
        }

        $output = "";
        $output .= $this ->ViewSubmission();

        $output .= render(
                    drupal_get_form(
                        'admin_webform_comment_submission_form',
                        $this ->_sid
                                        )
                );

        if(user_access('administer submissions')) {
                $output .= '<div id="administer-submission">' . $this ->_nid;
                $output .=
                render(drupal_get_form('admin_webform_admininster_submission_form', $this ->_nid, $this->_sid));
                $output .= '</div>';
        }

        return $output;
    }

    /**
     * public function CanUserAccess($user)
     * check if user can access to submission

     * @param $user {stdClass}
    *       - user object
     * @return {Bool}
     */
    public function CanUserAccess($user) {
            if(user_access('see all submissions', $user) === TRUE) {
                    return TRUE;
            }
            return in_array($user->uid, $this->_users);
    }

    /**
     * public function CanAddUser($uid) 
     * check if user id can be added to submission

     * @param $uid - user uid
     * @return booleand indicating if user can be added or not
     */
    public function CanAddUser($uid) {

            if($this ->UsersCount() == 3 || in_array($uid, $this ->_users)) {
                return false;
            }

            return true; 
    }

    /**
     *
     * Add user with check
     */
    public function AddUser($uid) {
            if($this ->CanAddUser($uid) == FALSE) {
                    return $this;
            }

            switch ($this ->_type) {
                    case  SINGLE_SUBMISSION: {
                            $this ->_users = array($uid, $uid, $uid);
                            break;
                    }
                    case  MULTIPLE_SUBMISSION: {
                            $this ->_users[] = $uid;
                            break;
                    }
            }

            return $this;
    }

    /**
     * Get submission from TABLE_NAME
     *@param $sid {int} - submission id
     *@return {stdClass} - submission structure 
     */
    public static function GetSubmission($sid) {
            $query = db_select( TABLE_NAME, 't');
            $query
            ->condition('sid', $sid)
            ->fields('t');

            $result = $query ->execute() ->fetchObject();

            if(empty($result)) {
                    return FALSE;
            }
            if(strlen($result ->users) > 0) {
                    $result ->users = explode(',', $result ->users);
            }
            else {
                    $result ->users = array();
            }
            return $result;
    }

    public static function IsSubmissionExists($sid) {
        return self::GetSubmission($sid) != FALSE;
    }

    public static function GetNid($sid) {
        $result = 
        db_select('webform_submissions', 'w')
        ->fields('w', array('nid'))
        ->condition('sid', $sid)
        ->execute()
        ->fetchField();

        return $result;
    }

    public function ChangeStatus($status) {
            if($status == 'post') {
                    $this ->Insert();
            }
            elseif($status == 'unpost') {
                    $this ->Delete();
            }

            return $this;
    }

    /**
     * insert submission in TABLE_NAME (post submission)
     */
    public function Insert() {
            if(!$this ->_isPosted) {
                    try {

                            db_insert( TABLE_NAME)
                            ->fields(array('sid' => $this->_sid, 'type' => $this->_type, 'users' => ''))
                            ->execute();
                            return $this;
                    }
                    catch(Exception $e) {
                            drupal_set_message("exeption occured $e");
                            return FALSE;
                    }
            }
    }

    /**
     * delete submission from TABLE_NAME (unpost submission)
     */
    public function Delete() {
            if($this ->_isPosted) {
                    try{
                            db_delete( TABLE_NAME)
                            ->condition('sid', $this ->_sid)
                            ->execute();
                            return $this;
                    }
                    catch(Exception $e) {
                            drupal_set_message("$e");

                            return FALSE;
                    }
            }
    }

    /**
     * Insert submission comment in TABLE_NAME. obvios
     */
    public function AddComment($comment) {
            try {
                    db_update( TABLE_NAME)
                    ->condition('sid', $this ->_sid)
                    ->fields(array('comment' => $comment))
                    ->execute();
                    return $this;
            }
            catch(Exception $e) {
                    drupal_set_message("$e");
            }
    }

    /**
     * public function Commit()
     * update changed of submission on database
     */
    public function Commit() {
        try {
            if($this ->_isPosted) {
                    $u = implode(',', $this ->_users);

                    db_update( TABLE_NAME)
                    ->condition('sid', $this ->_sid)
                    ->fields(array('type' => $this ->_type, 'users' => $u ))
                    ->execute();
            }
            else {
                    $this ->Insert();
            }

            return $this;
        }
        catch(Exception $e) {
            drupal_set_message("Exception occured wtile commiting. Contact site administrator : $e");
       }
    }
}

function admin_webform_submission($sid) {
    return new AdminWebformSubmission($sid);
}