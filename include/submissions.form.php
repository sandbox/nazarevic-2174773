<?php
function admin_webform_submissions_form($form, &$form_state) {
    ctools_add_js('main', 'admin_webform');
    
    $not_show = array(22);
    $form['nid'] = array(
        '#type' => 'select',
    );

    $form['nid']['#options'] = array();
    $views = variable_get('webform_mysql_views_views', array());

    $form['nid']['#options'][] = t("All"); 
    $nids = array();
    $keys = array_keys($views);
    foreach ($keys as $key) {
        if(in_array($key, $not_show)){continue;}

        $title = admin_webform_get_node_title($key);  
        $nids[] = $key;
        $form['nid']['#options'][$key] = $title;
    }

    $form['nid']['#attributes'] = array(
        'onchange' => 'jQuery("#-admin-webform-form").submit()',
    );

    $form['nid']['#default_value'] = admin_webform_get_from_session('last_nid', 0);

    $items_count = array(
        10, 20, 30, 50, 100,
    );

    $form['items-count'] = array(
        '#type' => 'select',
        '#options' => $items_count,
        '#attributes' => array(
            'onchange' => 'jQuery("#-admin-webform-form").submit()',
        ),
        '#default_value' => admin_webform_get_from_session("items_count", 0),
    );

    $form['submit'] = array(
        '#type' => 'submit',
        '#value' => 'Filter',
    );	

    $form['#submit'][] = 'admin_webform_webform_form_submit';

    $nid = admin_webform_get_from_session('last_nid');
    $count = $items_count[admin_webform_get_from_session('items_count')];
    $output = "";

    if($nid == 0) {
        $output = admin_webform_get_all_submissions($nids, $count).theme('pager');
    }
    else {		
        $output = admin_webform_create_table_from_view ($nid, $count);
    }

    $form['results'] = array(
        '#type' => 'fieldset',
        '#prefix' => '<div id="results">'.$output,
        '#suffix' => '</div>',
    );
    
    return $form;
}