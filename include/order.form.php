<?php
function admin_webform_submission_order_form($form, &$form_state,$sid) {    
    $form['sid'] = array(
        '#type' =>'hidden',
        '#default_value' => $sid,
    );
    global $u;
    $submission = new Submission($sid);
    
    $form['submission-type'] = array(
            '#type' => 'hidden',
            '#default_value' => $submission ->GetType(),
    );    
    
    if($submission ->IsNew()) {
        $form['submission-type'] = array(
            '#type' => 'radios',
            '#options' => array(
                300 => t('Single submission'),
                150 => t('Multiple submission'),
            ),
        );
    }
    
    if( $submission ->CanAddUser($u ->uid) ) {
        $form['buy-submission'] = array(
            '#type' => 'submit',
            '#value' => t("Buy"),
            '#ajax' => array(
                'callback' => 'admin_webform_buy_submission',
            ),
        );
    }
    else {
        $form['submission-closed'] = array(
            '#markup' => "<div>Submission closed</div>",
        );
    }
    
    return $form;
}

function admin_webform_buy_submission($form, &$form_state) {
    global $u;
    
    $submission = new Submission($form_state['values']['sid']);
    
    if($submission ->IsNew() || $submission ->GetType() == MULTIPLE_SUBMISSION) {
        $submission 
        ->SetType($form_state['values']['submission-type'])
        ->AddUser($u ->uid)
        ->Commit();
        drupal_set_message("success");        
    }
    else {
        drupal_set_message("Submission allready ordered");
    }       
   
    $commands = array();
    ctools_include('modal');
    
    $commands[] = ctools_modal_command_display('title', $submission ->ToHtml($u));
    
    return array('#type' => 'ajax', '#commands' => $commands);
}

